<p>Syst�me de gestion d'�tapes, partie HTML/CSS seulement, r�alis� avec la propri�t� flexbox de CSS3. </p>
<p>Incompatible avec les versions : </p>
<ul>
	<li>IE 9 et inf�rieures.            </li>
	<li>Firefox 21 et inf�rieures.   </li>
	<li>Chrome 20 et inf�rieures.  </li>
	<li>Op�ra 12.0 et inf�rieures.  </li>
	<li>Safari 3.0 et inf�rieures.    </li>
</ul>
<p>Responsive design.</p>